import torch
import torch.nn as nn
import numpy as np
from utils import ops
import torch.nn.functional as F
import torch.distributions as dist
from models import modules as Z
from models import discrete_prior as dp
from utils import visual_evaluation as viz


def f(in_channels, out_channels, hidden_channels):
    return nn.Sequential(
        Z.Conv2d(in_channels, hidden_channels), nn.ReLU(inplace=False),
        Z.Conv2d(hidden_channels, hidden_channels, kernel_size=1),
        nn.ReLU(inplace=False),
        Z.Conv2dZeros(hidden_channels, out_channels)
                        )


class FlowStep(nn.Module):
    def __init__(self, in_channles, hidden_channels, flow_permutation, flow_coupling):
        # A flow step involves actnorm, Permute, coupling
        super().__init__()
        # 1. actnorm
        self.actnorm = Z.ActNorm(in_channles)
        # 2. permutation
        if flow_permutation == 0:
            self.perm_m = Z.Permute2d(in_channles, shuffle=False)
        elif flow_permutation == 1:
            self.perm_m = Z.Permute2d(in_channles, shuffle=True)
        elif flow_permutation == 2:
            self.perm_m = Z.InvertibleConv1x1(in_channles)
        else:
            raise NotImplementedError('only values 0, 1 and 2 are supported')
        # 3. coupling layer
        if flow_coupling == 0:
            self.f = f(in_channles//2, in_channles//2, hidden_channels)
        elif flow_coupling == 1:
            self.f = f(in_channles//2, in_channles, hidden_channels)
        else:
            raise NotImplementedError('only values 0, 1 are supported')
        self.flow_coupling = flow_coupling
        self.nl = lambda x: torch.sigmoid(x + 2.)
        self.dlogdetB = None
        self.dlogdetF = None

    def forward(self, x, logdet=None, reverse=False):
        if not reverse:
            return self.forward_flow(x, logdet)
        else:
            return self.reverse_flow(x, logdet)

    def forward_flow(self, z, logdet):
        assert z.size(1) % 2 == 0
        # 1. actnorm
        z, logdet = self.actnorm(z, logdet, reverse=False)
        # 2. permutation
        z, logdet = self.perm_m(z, logdet, reverse=False)
        # 3. coupling
        z1, z2 = ops.split_feature(z, 'split')
        if self.flow_coupling == 0:
            z2 = z2 + self.f(z1)
        elif self.flow_coupling == 1:
            h = self.f(z1)
            shift, scale = ops.split_feature(h, 'cross')
            scale = self.nl(scale)
            z2 = (z2 + shift) * scale
            dlogdet = ops.sum(scale.log(), [1, 2, 3], keepdim=True)
            logdet += dlogdet
            self.dlogdetF = dlogdet.mean()
        z = torch.cat((z1, z2), 1)
        return z, logdet

    def reverse_flow(self, z, logdet):
        assert z.size(1) % 2 == 0
        # 3. coupling
        z1, z2 = ops.split_feature(z, 'split')
        if self.flow_coupling == 0:
            z2 = z2 - self.f(z1)
        elif self.flow_coupling == 1:
            h = self.f(z1)
            shift, scale = ops.split_feature(h, 'cross')
            scale = self.nl(scale)
            z2 = z2/scale - shift
            if logdet is not None:
                dlogdet = -ops.sum(scale, [1, 2, 3], keepdim=True)
                self.dlogdetB = dlogdet.mean()
                logdet += dlogdet
        z = torch.cat((z1, z2), 1)
        # 2. permute
        z, logdet = self.perm_m(z, logdet, reverse=True)
        # 3. actnorm
        z, logdet = self.actnorm(z, logdet, reverse=True)
        return z, logdet


class FlowNet(nn.Module):
    def __init__(self, args):

        """
                                 K

        -->[SqueezeLayer] -> [FlowStep] -> [Split2d] -> [SqueezeLayer]  -> [FlowStep]
                |                              |
                |           (L-1)              |
                |------------------------------|



        """
        super().__init__()
        self.K = args.depth
        self.L = args.n_levels
        self.hidden_channels = args.width
        self.flow_permutation = args.flow_permutation
        self.flow_coupling = args.flow_coupling
        self.keep_z2 = args.keep_z2

        h, w, c = args.image_shape
        assert c in [1, 3]
        # create a list of modules
        self.layers = nn.ModuleList()
        self.output_shapes = []
        factor = args.squeeze_f
        for i in range(self.L):
            # 1. SqueezeLayer
            self.layers.append(Z.SqueezeLayer(factor))
            c, h, w = c*factor*factor, h//factor, w//factor
            self.output_shapes.append([-1, c, h, w])
            # 2. K FlowSteps
            for _ in range(self.K):
                self.layers.append(FlowStep(in_channles=c,
                                            hidden_channels=self.hidden_channels,
                                            flow_permutation=self.flow_permutation,
                                            flow_coupling=self.flow_coupling))
                self.output_shapes.append([-1, c, h, w])

            # 3. Split2d
            # if False:
            if i < self.L-1:
                self.layers.append(Z.Split2d(c, args))
                c //= 2
                self.output_shapes.append([-1, c, h, w])

    def forward(self, z, logdet=None, reverse=False, eps_std=None):
        layers = self.layers
        if reverse:
            layers = layers[::-1]

        for m in layers:
            if isinstance(m, Z.Split2d):
                z, logdet = m(z, logdet, reverse=reverse, eps_std=eps_std,
                              keep_z2=self.keep_z2)
            else:
                z, logdet = m(z, logdet, reverse=reverse)

            if logdet is not None:
                assert ops.int_shape(logdet)[1:] == [1, 1, 1], \
                    f'broadcasiting issue at {m._get_name()} '

        return z, logdet


class Glow(nn.Module):
    BCE = nn.BCEWithLogitsLoss()
    # cross entropy with logits
    CE = nn.CrossEntropyLoss()

    def __init__(self, args):
        super().__init__()
        self.args = args
        self.flow = FlowNet(args)
        self.n_classes = args.n_classes
        self.batch_size = None

        if args.learn_top:
            c = self.flow.output_shapes[-1][1]
            self.learn_top = Z.Conv2dZeros(c*args.n_params, c*args.n_params)
        if args.y_cond:
            c = self.flow.output_shapes[-1][1]
            self.project2chan = Z.LinearZeros(args.n_classes, args.n_params*c)
            self.project2class = Z.LinearZeros(c, args.n_classes)
        self.dlogdetF = None

    def get_prior_params(self, y_onehot=None):
        c, h, w = self.flow.output_shapes[-1][1:]
        h = torch.zeros(self.batch_size, self.args.n_params*c, h, w,
                        requires_grad=False).to(self.args.device)
        if self.args.learn_top:
            h = self.learn_top(h)
        if self.args.y_cond:
            h = h + self.project2chan(y_onehot).view(-1, self.args.n_params*c, 1, 1)
        return h

    def forward(self, inp_, y_onehot=None, eps_std=None, reverse=False):

        if inp_ is not None:
            self.batch_size = inp_.size(0)
        elif y_onehot is not None:
            self.batch_size = y_onehot.size(0)
        else:
            self.batch_size = 100
        if not reverse:
            return self.forward_flow(inp_, y_onehot)
        else:
            return self.reverse_flow(inp_, y_onehot, eps_std)

    def forward_flow(self, x, y_onehot):
        b, c, h, w = ops.int_shape(x)
        n_pixels = h*w
        # add guassian noise
        # z = x + torch.normal(mean=torch.zeros_like(x),
        #         #                      std=torch.ones_like(x)*(1./256.)).to(x)
        # add uniform noise
        z = x + torch.rand(x.size())*1./256.
        z -= .5
        logdet = torch.zeros(b, 1, 1, 1).to(x)
        logdet += float(-np.log(256.)*n_pixels*c)

        # encode
        z, logdet = self.flow(z, logdet=logdet, reverse=False)
        features = self.get_prior_params(y_onehot)
        log_pz = Z.get_prior_dist(features, self.args, 'split').log_prob(z)
        log_pz = ops.sum(log_pz, [1, 2, 3], keepdim=True)
        self.dlogdetF = log_pz.mean()
        objective = logdet + log_pz

        if self.args.y_cond:
            y_logits = self.project2class(ops.mean(z, [2, 3]))
        else:
            y_logits = None
        # bits per dim loss
        bpd = -objective / float(np.log(2.)*n_pixels)
        assert (ops.int_shape(bpd)[1:] == [1, 1, 1]), \
            'something\'s wrong with broadcasting'

        return z, bpd, y_logits

    def reverse_flow(self, z, y_onehot, eps_std):
        with torch.no_grad():
            if z is None:
                features = self.get_prior_params(y_onehot)
                z = Z.get_prior_dist(features, self.args, 'split').sample()
                logdet = None
            else:
                logdet = torch.zeros(z.size(0), 1, 1, 1).to(z)
            x, logdet = self.flow(z, reverse=True, eps_std=eps_std,
                                  logdet=logdet)
            x += .5
        return x, logdet

    def manual_actnorm_init(self, initialized=True):
        for m in self.modules():
            if 'ActNorm' in m.__class__.__name__:
                m.initialized = initialized

    def generate_z(self, img):
        x = img.unsqueeze(0).to(self.args.device)
        z, _, _ = self.forward(x)
        self.train()
        return z.squeeze[0].detach().cpu().numpy()
    # TODO: write the generate attributes

    @staticmethod
    def loss_generative(bpd):
        # Generative loss
        return torch.mean(bpd)

    @staticmethod
    def loss_multi_classes(y_logits, y):
        if y_logits is None:
            return 0
        else:
            return Glow.CE(y_logits, y)

    @staticmethod
    def loss_class(y_logits, y):
        if y_logits is None:
            return 0
        else:
            return Glow.BCE(y_logits, y.long())






































