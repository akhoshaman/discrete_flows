import torch
import torch.nn as nn
import numpy as np
from utils import ops
import torch.nn.functional as F
import torch.distributions as dist
from models import discrete_prior as dp
from utils import visual_evaluation as viz


class ActNorm(nn.Module):
    counter = 0

    def __init__(self, num_features, in_dims=4, scale=1.0,
                 trainable=True, logscale_factor=3.):
        # TODO: see if it's needed to implemnent batch_variance from the
        # original implementation
        ActNorm.counter += 1
        self.counter = ActNorm.counter
        super().__init__()

        self.num_features = num_features

        self._set_dim_props(in_dims)
        self.bias = nn.Parameter(torch.zeros(*self.bias_dim), requires_grad=trainable)
        self.logs = nn.Parameter(torch.zeros(*self.bias_dim), requires_grad=trainable)

        self.scale = scale
        self.logscale_factor = logscale_factor

        self.initialized = False
        self.dlogdetB = None
        self.dlogdetF = None

    def _set_dim_props(self, in_dims):
        if in_dims == 4:
            self.bias_dim = [1, self.num_features, 1, 1]
            self._dims = [0, 2, 3]

        elif in_dims == 2:
            self.bias_dim = [1, self.num_features]
            self._dims = [0]
        else:
            raise ValueError('number of input dims should either be 2 or 4')

    def log_det_factor(self, x):
        x_shape = ops.int_shape(x)
        if len(x_shape) == 4:
            assert x_shape[1] == self.num_features
            return x_shape[2] * x_shape[3]

        elif len(x_shape) == 2:
            assert x_shape[1] == self.num_features
            return 1.

    def reset_parameters(self, x):
        if not self.training:
            pass
        assert x.device == self.bias.device
        with torch.no_grad():
            print(f'initializing ActNorm {self.counter}')
            bias = ops.mean(x.clone(), dim=self._dims, keepdim=True) * -1.
            var = ops.mean((x.clone() + bias) ** 2, dim=self._dims, keepdim=True)
            log_scale = torch.log(self.scale / (torch.sqrt(var) + 1e-6))
            log_scale /= self.logscale_factor
            self.bias.data.copy_(bias.data)
            self.logs.data.copy_(log_scale.data)
            self.initialized = True

    def _center(self, x, reverse=False):
        if not reverse:
            return x + self.bias
        else:
            return x - self.bias

    def _scale(self, x, logdet=None, reverse=False):
        logdet_fac = self.log_det_factor(x)
        logs = self.logs * self.logscale_factor
        if not reverse:
            x = x * torch.exp(logs)
        else:
            x = x * torch.exp(-logs)

        dlogdet = logdet_fac * self.logs.sum()
        if logdet is not None:
            if reverse:
                dlogdet = dlogdet * -1.
                self.dlogdetB = dlogdet.mean()
            else:
                self.dlogdetF = dlogdet.mean()
            logdet = logdet + dlogdet
        return x, logdet

    def forward(self, x, logdet=None, reverse=False):
        # TODO: remove this line
        # return x, logdet
        if not self.initialized:
            self.reset_parameters(x)
        if not reverse:
            x = self._center(x, reverse)
            x, logdet = self._scale(x, logdet, reverse)
        else:
            x, logdet = self._scale(x, logdet, reverse)
            x = self._center(x, reverse)
        return x, logdet


class LinearZeros(nn.Linear):
    def __init__(self, in_features, out_features, logscale_factor=3.):
        super().__init__(in_features, out_features)
        self.logs = nn.Parameter(torch.zeros(out_features))
        self.logscale_factor = logscale_factor
        self.bias.data.zero_()
        self.weight.data.zero_()

    def forward(self, x):
        out = super(LinearZeros, self).forward(x)
        return out * torch.exp(self.logs * self.logscale_factor)


class Conv2d(nn.Conv2d):

    def __init__(self, in_channels, out_channels,
                 kernel_size=3, stride=1,
                 padding="same", do_actnorm=True, weight_std=0.05, context1d=None,
                 skip=1, edge_bias=True):
        #  TODO: implement context and skip

        assert isinstance(stride, int)
        assert isinstance(kernel_size, int)
        assert isinstance(padding, str)
        assert stride == 1, 'only stride 1 is supported'
        self.padding = padding
        # note that we initialize the parent class with 0 padding to do manual padding later
        super().__init__(in_channels + int(edge_bias),
                         out_channels, kernel_size, stride,
                         padding=0, bias=(not do_actnorm))

        # init weight with std
        self.weight.data.normal_(mean=0.0, std=weight_std)
        if not do_actnorm:
            self.bias.data.zero_()
        else:
            self.actnorm = ActNorm(out_channels)
        self.do_actnorm = do_actnorm
        self.edge_chan = None
        self.edge_bias = edge_bias
        self.kernel_size = kernel_size
        self.pad_m_1 = nn.ConstantPad2d((self.kernel_size - 1) // 2, 1.)
        self.pad_m_0 = nn.ConstantPad2d((self.kernel_size - 1) // 2, 0.)

    def forward(self, x):
        x = _add_edge_chan(self, x)
        x = super().forward(x)
        if self.do_actnorm:
            x, _ = self.actnorm(x)
        return x


def _add_edge_chan(cl, x):
    # create the pad channel if it's not created (cl.edge_chan is None)
    if cl.kernel_size == 1:
        return x
    if (cl.edge_chan is None) and cl.edge_bias:
        s = ops.int_shape(x)
        s[1] = 1
        pad = torch.zeros(s).to(x)
        cl.edge_chan = cl.pad_m_1(pad)
    # apply the regular padding to x
    x = cl.pad_m_0(x)
    # concat the extra edge channel
    if cl.edge_bias:
        x = torch.cat([x, cl.edge_chan], 1)
    return x


class Conv2dZeros(nn.Conv2d):

    def __init__(self, in_channels, out_channels,
                 kernel_size=3, stride=1,
                 padding="same", logscale_factor=3.,
                 skip=1, edge_bias=True):
        #  TODO: implement skip

        assert isinstance(stride, int)
        assert isinstance(kernel_size, int)
        assert isinstance(padding, str)
        assert stride == 1, 'only stride 1 is supported'
        self.padding = padding
        # note that we initialize the parent class with 0 padding to do manual padding later
        super().__init__(in_channels + int(edge_bias),
                         out_channels, kernel_size, stride,
                         padding=0)

        self.logs = nn.Parameter(torch.zeros(1, out_channels, 1, 1))
        # init weight with zero
        self.weight.data.zero_()

        self.edge_chan = None
        self.edge_bias = edge_bias
        self.kernel_size = kernel_size
        self.logscale_factor = logscale_factor
        self.pad_m_1 = nn.ConstantPad2d((self.kernel_size - 1) // 2, 1.)
        self.pad_m_0 = nn.ConstantPad2d((self.kernel_size - 1) // 2, 0.)

    def forward(self, x):
        x = _add_edge_chan(self, x)
        x = super().forward(x)
        return x * torch.exp(self.logs * self.logscale_factor)


class Permute2d(nn.Module):
    """  shuffles or reversed the ordering of the channels"""

    def __init__(self, in_channels, shuffle=False):
        super().__init__()
        self.in_channels = in_channels
        self.indices = np.arange(in_channels)
        if shuffle:
            np.random.shuffle(self.indices)
        else:
            self.indices = np.arange(in_channels)[::-1]
        self.indices_inverse = np.argsort(self.indices).tolist()
        self.indices = self.indices.tolist()

    def forward(self, x, logdet=None, reverse=False):
        assert len(x.size()) == 4
        if not reverse:
            return x[:, self.indices, :, :], logdet
        else:
            return x[:, self.indices_inverse, :, :], logdet


class InvertibleConv1x1(nn.Module):
    # not using LU decomposition, since it doesn't make much of a difference
    def __init__(self, in_channels, lu=False):
        super().__init__()
        w_init, _ = np.linalg.qr(np.random.randn(in_channels, in_channels))
        self.lu = lu
        if not lu:
            self.weight = nn.Parameter(torch.tensor(w_init.astype(np.float32)))
        else:
            raise NotImplementedError()
        self.dlogdetB = None
        self.dlogdetF = None

    def get_weight(self, x, reverse):
        shape = ops.int_shape(x)
        if not self.lu:
            logdet = self.weight.double().det().abs().log().float() \
                     * shape[2] * shape[3]
        else:
            raise NotImplementedError()
        if not reverse:
            weight = self.weight.unsqueeze(2).unsqueeze(3)
        else:
            weight = torch.inverse(self.weight.transpose(1, 0).double())\
                .unsqueeze(2).unsqueeze(3).float()
        return weight, logdet

    def forward(self, x, logdet=None, reverse=False):
        weight, dlogdet = self.get_weight(x, reverse)
        z = F.conv2d(x, weight)
        if reverse:
            dlogdet = dlogdet * -1

        if logdet is not None:
            logdet = logdet + dlogdet
        if reverse:
            self.dlogdetB = dlogdet.mean()
        else:
            self.dlogdetF = dlogdet.mean()

        return z, logdet


class Split2d(nn.Module):
    def __init__(self, in_channels, args):
        super().__init__()
        self.conv = Conv2dZeros(in_channels // 2, in_channels // 2 * args.n_params)
        self.z2 = None
        self.args = args
        self.dlogdetB = None
        self.dlogdetF = None

    def forward(self, z, logdet=None, reverse=False,
                eps_std=None, keep_z2=False):
        if not reverse:
            # take z, discard half (z2) and return z1
            z1, z2 = ops.split_feature(z, 'split')
            if keep_z2:
                self.z2 = z2.data
            # z2 is discarded from the rest of the flow and its parameters are calculated
            # get parameters of z2
            features = self.conv(z1)
            prior = get_prior_dist(features, self.args, 'cross')

            if logdet is not None:
                dlogdet = ops.sum(prior.log_prob(z2),
                                          [1, 2, 3], keepdim=True)
                logdet += dlogdet
                self.dlogdetF = dlogdet.mean()
                if self.dlogdetF > 0.:
                    print('prior collapse')
            return z1, logdet
        else:
            # take z1 and return z = concat(z1,z2)
            # get parameters of z2
            z1 = z
            features = self.conv(z1)
            prior = get_prior_dist(features, self.args, 'cross')

            if keep_z2:
                print('using the saved value of z2 in Split2d')
                z2 = self.z2
            else:
                z2 = prior.sample()
            z = torch.cat((z1, z2), 1)
            if logdet is not None:
                dlogdet = -ops.sum(prior.log_prob(z2),
                                          [1, 2, 3], keepdim=True)
                logdet += dlogdet
                self.dlogdetB = dlogdet.mean()
            return z, logdet


def squeeze2d(x, f=2):
    # converts (b, c, h, w) -> (b, c*f*f, h//f. w//f)
    assert f >= 1 and isinstance(f, int)
    if f == 1:
        return x
    b, c, h, w = ops.int_shape(x)
    assert h % f == 0 and w % f == 0, f'{(h, w)} not divisible by {f}'
    x = x.view(b, c, h // f, f, w // f, f)
    #         (0, 1,    2,   3,  4,     5)
    x = x.permute(0, 1, 3, 5, 2, 4).contiguous()
    return x.view(b, c * f * f, h // f, w // f)


def unsqueeze2d(x, f=2):
    # converts  (b, c*f*f, h//f. w//f) -> (b, c, h, w)
    assert f >= 1 and isinstance(f, int)
    if f == 1:
        return x
    b, cff, hof, wof = ops.int_shape(x)
    assert cff % f * f == 0
    x = x.view(b, cff // (f * f), f, f, hof, wof)
    #         (0,      1,         2, 3,  4,   5)
    x = x.permute(0, 1, 4, 2, 5, 3).contiguous()
    return x.view(b, cff // (f * f), hof * f, wof * f)


class SqueezeLayer(nn.Module):
    def __init__(self, factor):
        super().__init__()
        self.factor = factor

    def forward(self, x, logdet=None, reverse=False):
        if not reverse:
            x = squeeze2d(x, self.factor)
        else:
            x = unsqueeze2d(x, self.factor)
        return x, logdet


def get_prior_dist(features, args, method, eps_std=None):
    if args.prior_name.lower() == 'gaussiandiag':
        mean, logs = ops.split_feature(features, method, args.n_g)
        std = logs.exp()
        if eps_std is not None:
            std = std * eps_std
        return dist.Normal(mean, std)

    elif args.prior_name.lower() == 'discretefac':
        mean, logs, logits = ops.split_feature(features, method, args.n_g)
        return dp.DiscreteFac(mean, logs, logits, args.k)

    else:
        raise NotImplementedError(f'distribution {args.prior_name.lower()}'
                                  f' not supported!')


def get_n_params_prior(args):
    if args.prior_name.lower() == 'gaussiandiag':
        n_param_groups = 2
        n_params = n_param_groups

    elif args.prior_name.lower() == 'discretefac':
        n_param_groups = 3
        n_params = n_param_groups * args.k

    else:
        raise NotImplementedError(f'distribution {args.prior_name.lower()}'
                                  f' not supported!')
    return n_param_groups, n_params
