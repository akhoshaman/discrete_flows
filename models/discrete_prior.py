import torch
import torch.nn as nn
import torch.distributions as dist
from utils import ops
import numpy as np


class DiscreteFac:
    def __init__(self, means, logs, logits, k):
        shape = ops.int_shape(means)
        n_dim = len(shape)
        assert n_dim in [4]
        nxk = np.prod(shape[1:])
        assert nxk % k == 0
        n_z = nxk // k
        # make the param's shape (b, n_z, k)
        means = means.view(-1, n_z, k)
        logs = logs.view_as(means)
        logits = logits.view_as(means)
        scale = logs.exp()

        self.n_z = n_z
        self.k = k

        self.normal = dist.Normal(loc=means, scale=scale)
        self.categ = dist.Categorical(logits=logits)
        self.logits = logits
        self.loc = means
        self.scale = scale

    def log_prob(self, zeta, sum_dims=False):
        shape = ops.int_shape(zeta)
        assert len(shape) in [4]
        if len(shape) == 4:
            # make zeta's shape (b, n_z, 1)
            zeta = ops.flatten(zeta)
            zeta = zeta.unsqueeze(2)
        post_logp = self.normal.log_prob(zeta)
        log_res = torch.log_softmax(self.logits, -1)
        # --> (b, n_z)
        log_p = torch.logsumexp(post_logp+log_res, -1)
        if sum_dims:
            return log_p.sum(1)
        else:
            return log_p.view(*shape)

    def sample(self):
        z = self.categ.sample().unsqueeze(2)  # -> (b, n_z, 1)
        # loc[i, j, k] = self.loc[i, j, z[i, j, k]]
        loc = torch.gather(input=self.loc, dim=2, index=z)
        scale = torch.gather(self.scale, 2, z)
        return dist.Normal(loc, scale).sample().squeeze(2)


class DiscreteFactorialSparse(nn.Module):
    def __init__(self, k, n_z, train_cont=True, train_disc=True):
        super().__init__()
        # zeta and z are the continuous variables. k is the number of categories. n_z is the dimensionality of z.
        self.k = k
        self.n_z = n_z
        # parameters of zeta
        self.register_parameter('loc', nn.Parameter(torch.linspace(-1., 1., k), requires_grad=train_cont))
        self.register_parameter('scale', nn.Parameter(torch.ones(k), requires_grad=train_cont))
        # parameters of z
        self.register_parameter('pi_logits', nn.Parameter(torch.randn(n_z, k), requires_grad=train_disc))
        self.z_dist = dist.Categorical(logits=self.pi_logits)
        # f is the distribution of zeta given z
        self.f = dist.Normal(torch.tensor(self.loc), torch.tensor(self.scale))

    def sample(self, sample_size, zeta_only=True):
        with torch.no_grad():
            z = self.z_dist.sample(sample_size)
            loc = self.loc[z.long()]
            scale = self.scale[z.long()]
            zeta = dist.Normal(loc, scale).sample()
        if zeta_only:
            return zeta
        return torch.cat((zeta, z), 1)

    def log_prob(self, zeta):
        shape = zeta.size()
        assert shape.__len__() == 2
        assert shape[-1] == self.n_z
        batch_size = shape[0]
        f_logp = self.f.log_prob(zeta.view(-1, 1))
        logits = self.pi_logits.unsqueeze(0).expand(batch_size, self.n_z, self.k).contiguous().view(f_logp.size())
        log_p_zeta = torch.logsumexp(f_logp + torch.log_softmax(logits, -1), -1).view(batch_size, self.n_z)
        log_p_zeta = log_p_zeta.sum(1)
        return log_p_zeta

    def forward(self, *input):
        pass
