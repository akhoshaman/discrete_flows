#!/usr/bin/env bash
python train_mnist.py --weight_y .1 --depth 2 --n_levels 2  \
--flow_coupling 0 --flow_permutation 2 --img_to_tensorboard --signature "glow-nice"\
 --width 128 --resize True --squeeze_f 2 --keep_z2
# --learn_top --y_cond
