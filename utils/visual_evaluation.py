# import matplotlib
import numpy as np
import torch
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
from utils import util

import torchvision.utils as vutils


def plot_images(x_sample, dir=None, file_name=None, n_sample=None, bianary=True):
    if torch.is_tensor(x_sample):
        x_sample = x_sample.detach().cpu().numpy()
    if n_sample is None:
        n_sample = x_sample.shape[0]
    size_x = int(np.floor(np.sqrt(n_sample)))
    fig = plt.figure(figsize=(size_x, size_x))
    gs = gridspec.GridSpec(size_x, size_x)
    gs.update(wspace=0.05, hspace=0.05)

    for i, sample in enumerate(x_sample[:size_x**2]):
        ax = plt.subplot(gs[i])
        plt.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        sample = sample.transpose(1, 2, 0)
        if sample.shape[2] == 1:
            sample = sample[:, :, 0]
            if bianary:
                plt.imshow(sample, cmap='gray', vmin=0, vmax=1)
            else:
                plt.imshow(sample)
                plt.colorbar()
        else:
            plt.imshow(sample)

    if dir is not None:
        util.makedirs(dir)
        plt.savefig(os.path.join(dir, file_name+'.png'), bbox_inches='tight')
        plt.close(fig)
    else:
        plt.show()


def img_to_tensorboard(img, writer, name, step):
    x = vutils.make_grid(img, normalize=True, scale_each=True)
    writer.add_image(name, x, step)
