import torch
import torch.nn as nn
import numpy as np
import collections
from itertools import repeat


def sum(tensor, dim=None, keepdim=False):
    """multi-axis sum.
    WARNING: this function changes the tensor.
     So make sure to clone it, if you need the original tensor
    """
    if dim is None:
        # sum up all dim
        return torch.sum(tensor)
    else:
        if isinstance(dim, int):
            dim = [dim]
        dim = sorted(dim)
        for d in dim:
            tensor = tensor.sum(dim=d, keepdim=True)
        if not keepdim:
            for i, d in enumerate(dim):
                tensor.squeeze_(d-i)
        return tensor


def mean(tensor, dim=None, keepdim=False):
    if dim is None:
        # mean all dim
        return torch.mean(tensor)
    else:
        if isinstance(dim, int):
            dim = [dim]
        dim = sorted(dim)
        for d in dim:
            tensor = tensor.mean(dim=d, keepdim=True)
        if not keepdim:
            for i, d in enumerate(dim):
                tensor.squeeze_(d-i)
        return tensor


def int_shape(x):
    return [int(s) for s in x.size()]


def split_feature(tensor, method="split", n_chunks=2):
    """
    method = ["split", "cross"]
    """
    c = tensor.size(1)
    chunks = []
    step = c // n_chunks
    assert c % n_chunks == 0
    for i in range(n_chunks):
        if method == 'split':
            chunks.append(tensor[:, i*step:(i+1)*step, ...])
        elif method == "cross":
            chunks.append(tensor[:, i::n_chunks, ...].contiguous())
        else:
            raise NotImplementedError()
    return chunks


def one_hot(y, num_classes):
    y_onehot = torch.zeros(y.size(0), num_classes).to(y.device)
    if len(y.size()) == 1:
        y_onehot = y_onehot.scatter_(1, y.unsqueeze(-1), 1)
    elif len(y.size()) == 2:
        y_onehot = y_onehot.scatter_(1, y, 1)
    else:
        raise ValueError("[one_hot]: y should be in shape [B], or [B, C]")
    return y_onehot


def flatten(x):
    shape = int_shape(x)
    return x.view(shape[0], -1)


if __name__ == '__main__':
    x = torch.empty(3, dtype=torch.long).random_(5)
    print(f'original x is {x} \n and the one_hot version is\n {one_hot(x, 5)}')


