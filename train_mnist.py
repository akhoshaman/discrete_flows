import torch
import torch.nn as nn
import numpy as np
from utils import ops
from utils import util
from models import modules as Z
from models import models, discrete_prior as dp
from datasets import dset
import argparse
import os
from tensorboardX import SummaryWriter
from utils import visual_evaluation as viz
import datetime

parser = argparse.ArgumentParser()

parser.add_argument('--signature', type=str, default='')
parser.add_argument('--add_time_stamp', action='store_true',
                    help="make unique directory name")
# dataset params
parser.add_argument('--data_set', type=str,
                    choices=['mnist', 'cifar10'], default='mnist')
parser.add_argument('--data_aug', type=eval, default=True,
                    choices=[True, False])
parser.add_argument('--resize', type=eval, default=True,
                    choices=[True, False])

# training params
parser.add_argument('--gpu', type=int, default=0)
parser.add_argument('--lr', type=float, default=0.001)
parser.add_argument('--max_grad_clip', type=float, default=5.)
parser.add_argument('--max_grad_norm', type=float, default=100.)

parser.add_argument('--batch_size', type=int, default=128)
parser.add_argument('--test_batch_size', type=int, default=1000)
parser.add_argument('--epochs', type=int, default=1600)
parser.add_argument('--gen_data_every', type=int, default=100,
                    help='generate data at every ... steps')
parser.add_argument('--evaluate_every', type=int, default=100,
                    help='evaluate the model at every ... steps')
parser.add_argument('--training_stats_every', type=int, default=10,
                    help='show training states at every ... steps')
parser.add_argument('--visualize_every', type=int, default=300,
                    help='visualize at every ... steps')
parser.add_argument('--n_gen', type=int, default=9, 
                    help='# of images to visualize')


# network params
parser.add_argument('--depth', type=int, default=12,
                    help="number of flow steps (actnorm, permutation and "
                         "affine coupling) in each layer")
parser.add_argument('--n_levels', type=int, default=1,
                    help="number of layers (squeeze, flow_step, split)")
parser.add_argument("--width", type=int, default=4,
                    help="Width of hidden layers")

parser.add_argument('--flow_permutation', type=int, default=1, choices=[0, 1, 2],
                    help="0->reverse , 1->permute 2-> conv1x1 the channels ")

parser.add_argument('--flow_coupling', type=int, default=1, choices=[0, 1],
                    help="0->NICE , 1->realNVP ")

parser.add_argument('--keep_z2', action='store_true',
                    help="remember z2 to make sure that the network during "
                         "splitting remains bijective ")

# debugging params
parser.add_argument('--squeeze_f', type=int, default=2,
                    help="the fraction to squeeze spatial dims")

# prior params
parser.add_argument('--prior_name', type=str, default='gaussiandiag',
                    choices=['discretefac', 'gaussiandiag'])
parser.add_argument('--k', type=int, default=1,
                    help=' # of categories in the discrete case')
parser.add_argument('--learn_top', action='store_true',
                    help='learn the parameters of the prior')
parser.add_argument('--y_cond', action='store_true',
                    help='condition the parameters of the prior on the labels')

parser.add_argument("--weight_y", type=float, default=0.00,
                    help="Weight of log p(y|x) in weighted loss")

# checkpoint params
parser.add_argument('--save', type=str, default='../checkpoints',
                    help='checkpoint folder')
parser.add_argument('--img_to_tensorboard', action='store_true')
parser.add_argument('--save_png_img', action='store_true')
parser.add_argument('--debug', action='store_true')


args = parser.parse_args()

if args.data_set == 'mnist':
    args.image_shape = [32, 32, 1] if args.resize else [28, 28, 1]
    args.n_classes = 10
elif args.data_set == 'cifar10':
    args.image_shape = [32, 32, 3]
    args.n_classes = 10

else:
    raise NotImplementedError(f'dataset {args.data_set} not implemented.')

args.n_g, args.n_params = Z.get_n_params_prior(args)

if args.prior_name == 'gaussiandiag' and args.k != 1:
    parser.error('the number of categories are equal to 1 for gaussian prior!')

if __name__ == '__main__':

    args.save = os.path.join(args.save,
                             args.signature +
                             str(datetime.datetime.now())[0:19].replace(' ', '_')
                             if args.add_time_stamp else ''
                             )

    util.makedirs(args.save)
    logger = util.get_logger(logpath=os.path.join(args.save, 'logs'),
                             filepath=os.path.abspath(__file__))
    logger.info(args)
    device = torch.device('cuda:' + str(args.gpu) if torch.cuda.is_available() else 'cpu')
    args.device = device
    model = models.Glow(args).to(device)
    writer = SummaryWriter(log_dir=args.save)
    logger.info(model)
    logger.info('Number of parameters: {}'.format(util.count_parameters(model)))

    dsetloader ={'mnist': dset.get_mnist_loaders,
                 'cifar10': dset.get_cifar_loaders}[args.data_set]

    train_loader, test_loader, train_eval_loader = dsetloader(
        args.data_aug, args.batch_size, args.test_batch_size, args.resize)

    data_gen = dset.inf_generator(train_loader)
    batches_per_epoch = len(train_loader)
    n_steps = args.epochs * batches_per_epoch

    optimizer = torch.optim.Adam(model.parameters(), args.lr)

    for step in range(n_steps):
        x, y = data_gen.__next__()
        x = x.to(device)
        y = y.to(device)
        y_onehot = ops.one_hot(y, args.n_classes)
        # initialize ActNorm
        if step == 0:
            # model(x, y_onehot)
            logger.info(f'running on {device}')
            z, bpd, y_logits = model(x, y_onehot)
            x_, logdet_ = model(z, y_onehot, reverse=True)
            diff_x_x__ = (x - x_).norm(1).data.cpu().numpy()
            logger.info(f'original ||x-x_||={diff_x_x__}')

        optimizer.zero_grad()
        z, bpd, y_logits = model(x, y_onehot)
        gen_loss = bpd.mean()
        disc_loss = 0.
        if args.y_cond:
            disc_loss = model.loss_multi_classes(y_logits, y)

        loss = gen_loss + args.weight_y * disc_loss
        loss.backward()
        # operate grad
        if args.max_grad_clip is not None and args.max_grad_clip > 0:
            torch.nn.utils.clip_grad_value_(model.parameters(),
                                            args.max_grad_clip)
        if args.max_grad_norm is not None and args.max_grad_norm > 0:
            grad_norm = torch.nn.utils.clip_grad_norm_(model.parameters(),
                                                       args.max_grad_norm)
            if step % args.training_stats_every == 0:
                logger.info(f'grad_norm at step {step} is {grad_norm}')

        optimizer.step()
        writer.add_scalar('loss/gen_loss', gen_loss, step)
        writer.add_scalar('loss/disc_loss', disc_loss, step)

        if step % args.training_stats_every == 0:
            with torch.no_grad():
                logger.info(f'step{step}/{n_steps}')
                logger.info(f"""generative loss: {gen_loss:.3f},
                            discriminative loss: {disc_loss:.3f}, 
                            total loss: {loss:.3f}""")

                z, bpd, y_logits = model(x, y_onehot)
                x_, bpd2 = model(z, y_onehot, reverse=True)
                diff_x_x__ = (x - x_).norm(1).data.cpu().numpy()
                logger.info(f'||x-x_||={diff_x_x__}')
                writer.add_scalar('loss/diff_x_x__', diff_x_x__, step)

                model.flow.keep_z2 = False
                x_gen, _ = model(inp_=None, y_onehot=y_onehot, reverse=True)
                model.flow.keep_z2 = args.keep_z2
                for ind, m in enumerate(model.modules()):
                    if hasattr(m, 'dlogdetF') and m.dlogdetF is not None:
                        namef = m.__repr__().split('(')[0] + f'F{ind}'
                        # nameb = m.__repr__().split('(')[0] + f'B{ind}'
                        writer.add_scalar(namef, m.dlogdetF, step)
                        logger.info(f'{namef}, {m.dlogdetF}, {step}')

                # for name, param in model.named_parameters():
                #     print(name, param.grad.mean())

                mean_x_ = x_.mean().data.cpu().numpy()
                mean_x_gen = x_gen.mean().data.cpu().numpy()
                logger.info(f'mean_x_, {mean_x_}')
                logger.info(f'mean_x_gen, {mean_x_gen}')
                writer.add_scalar('mean_x_', mean_x_, step)
                writer.add_scalar('x_gen_mean', mean_x_gen, step)
                if np.isnan(mean_x_gen) or np.abs(mean_x_gen) > .2:
                    print('got nan!')
                logger.info('--' * 30)

        if step % args.visualize_every == 0:
            with torch.no_grad():
                z, bpd, y_logits = model(x, y_onehot)
                x_, bpd2 = model(z, y_onehot, reverse=True)
                diff_x_x__ = (x - x_).norm(1).data.cpu().numpy()
                logger.info(f'||x-x_||={diff_x_x__}')
                writer.add_scalar('loss/diff_x_x__', diff_x_x__, step)

                model.flow.keep_z2 = False
                x_gen, _ = model(inp_=None, y_onehot=y_onehot, reverse=True)
                model.flow.keep_z2 = args.keep_z2
                if args.img_to_tensorboard or args.save_png_img:
                    # z_unsqueeze = Z.unsqueeze2d(z, 2)
                    if args.save_png_img:
                        viz.plot_images(x, args.save, f'{step}x', args.n_gen)
                        # viz.plot_images(z_unsqueeze, args.save,
                        #                 f'{step}z', args.n_gen, bianary=False)
                        viz.plot_images(x_, args.save, f'{step}x_', args.n_gen)
                        viz.plot_images(x_gen, args.save, f'{step}x_gen', args.n_gen)
                    if args.img_to_tensorboard:
                        viz.img_to_tensorboard(x[:args.n_gen], writer, 'x', step)
                        # viz.img_to_tensorboard(z_unsqueeze.detach()[:args.n_gen],
                        #                        writer, 'z', step)
                        viz.img_to_tensorboard(x_[:args.n_gen], writer, 'x_', step)
                        viz.img_to_tensorboard(x_gen[:args.n_gen], writer, 'x_gen', step)

    writer.export_scalars_to_json(os.path.join(args.save, "all_scalars.json"))
    writer.close()

# TODO: check gradients throughout
